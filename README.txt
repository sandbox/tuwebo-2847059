Provides a permission to control whether or not users can view their own
account.

If the user does not have the "View own user account" permission, the "View"
local menu option on the user's profile will be hidden. If the user attempts to
view their own profile by going to /user they will recieve an access
denied page.
